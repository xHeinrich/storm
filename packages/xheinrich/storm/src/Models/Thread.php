<?php

namespace xHeinrich\Storm\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    public function user()
    {
    	return $this->belongsTo(config('storm.user_model'));
    }
    
	public function category()
	{
		return $this->belongsTo(xHeinrich\Storm\Models\Category::class);
	}

	public function posts()
	{
		return $this->hasMany(xHeinrich\Storm\Models\Post::class);
	}

}
