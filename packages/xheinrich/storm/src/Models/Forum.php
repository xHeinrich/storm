<?php

namespace xHeinrich\Storm\Models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    
	public function categories()
	{
		return $this->hasMany(xHeinrich\Storm\Models\Category::class);
	}

}
