<?php

namespace xHeinrich\Storm;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class StormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/resources/routes/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'storm');
        $this->mergeConfigFrom(
            __DIR__.'/resources/config/storm.php', 'storm'
        );
        $this->registerControllers();
        $this->publish();
        $this->modelBinding();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function publish()
    {
        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/storm'),
        ]);
        $this->publishes([
            __DIR__.'/resources/config/storm.php' => config_path('storm.php')
        ], 'config');
        $this->publishes([
            __DIR__.'/resources/database/migrations/' => database_path('migrations')
        ], 'migrations'); 
    }

    public function registerControllers()
    {
        $this->app->make('xHeinrich\Storm\Http\Controllers\ForumController');
        $this->app->make('xHeinrich\Storm\Http\Controllers\CategoryController');
        $this->app->make('xHeinrich\Storm\Http\Controllers\PostController');
        $this->app->make('xHeinrich\Storm\Http\Controllers\ThreadController');
    }

    public function observeModels()
    {
        Thread::observe(new ThreadObserver);
        Post::observe(new PostObserver);
    }

    public function modelBinding()
    {
        Route::bind('category', function($value){
            return \xHeinrich\Storm\Models\Category::where('thread_slug', $value)->first();
        });
        Route::bind('thread', function($value){
            return \xHeinrich\Storm\Models\Thread::where('thread_slug', $value)->first();
        });
        Route::model('post', \xHeinrich\Storm\Models\Post::class);

    }
}
