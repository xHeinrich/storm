<?php

namespace xHeinrich\Storm\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use xHeinrich\Storm\Models\Category;
use xHeinrich\Storm\Models\Forum;

class CategoryController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @param  \xHeinrich\Storm\Models\Forum    $forum
     * @return \Illuminate\Http\Response
     */
    public function index(Forum $forum)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \xHeinrich\Storm\Models\Forum    $forum
     * @return \Illuminate\Http\Response
     */
    public function create(Forum $forum)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \xHeinrich\Storm\Models\Category  $categoryForum
     * @param  \xHeinrich\Storm\Models\Forum    $forum
     * @return \Illuminate\Http\Response
     */
    public function show(Forum $forum, Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \xHeinrich\Storm\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum, Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \xHeinrich\Storm\Models\Forum    $forum
     * @param  \xHeinrich\Storm\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forum $forum, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \xHeinrich\Storm\Models\Forum    $forum
     * @param  \xHeinrich\Storm\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forum $forum, Category $category)
    {
        //
    }
}
