<?php

namespace xHeinrich\Storm\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    


	public function threads()
	{
		return $this->hasMany(xHeinrich\Storm\Models\Thread::class);
	}
	
}
