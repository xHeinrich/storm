<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('storm.table_prefix') . 'threads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id'); //initial post
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->boolean('stickied');
            $table->boolean('locked');
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('storm.table_prefix') . 'threads');
    }
}
