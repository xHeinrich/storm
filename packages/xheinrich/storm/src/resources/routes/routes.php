<?php
	Route::resource('forum', 'xHeinrich\Storm\Http\Controllers\ForumController');
	Route::resource('forum.category', 'xHeinrich\Storm\Http\Controllers\CategoryController');
	Route::resource('category.thread', 'xHeinrich\Storm\Http\Controllers\ThreadController');
	Route::resource('post', 'xHeinrich\Storm\Http\Controllers\PostController');
