<?php

namespace xHeinrich\Storm\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	public function user()
	{
		return $this->belongsTo(config('storm.user_model'));
	}

	public function thread()
	{
		return $this->belongsTo(xHeinrich\Storm\Models\Thread::class);
	}

}
